package com.a1.example.nchacon.asistiwa;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView texto;
    EditText usuario,contraseña;
    Button entrar;


//usuario / contraseñas
    String[] usuaios={"iwa","Axel","Carlos","Emanuel","Frank",
                              "Gerardo","Hector","Julio","Norberto","Sergio"};
    String[] contraseñas={"demo","agutierrez","cperez","ebautista","fperez",
            "garellano","hpolanco","jduran","nchacon","sduran"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //textview
        texto=(TextView)findViewById(R.id.saludar);
        //edit text
        usuario=(EditText)findViewById(R.id.user);
        contraseña=(EditText)findViewById(R.id.pw);
        //botones
        entrar=(Button)findViewById(R.id.login);

        entrar.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {


        switch (view.getId()){
            case R.id.login:

                String users=usuario.getText().toString();
                String pws=contraseña.getText().toString();
                if(!users.equalsIgnoreCase("") && !pws.equalsIgnoreCase(""))
                {
                    //inicia revisar conexion de red
                    ConnectivityManager revisarconexion=(ConnectivityManager)
                        getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo netinfo=revisarconexion.getActiveNetworkInfo();
                    //si esta conectada a red
                    if(netinfo != null && netinfo.isConnected())
                    {
                        boolean existe=false;
                        for(int i=0;i<usuaios.length;i++)
                        {
                            for (int j=0;j<contraseñas.length;j++)
                            {
                                if(users.equals(usuaios[i])&& pws.equals(contraseñas[j]))
                                {
                                    existe=true;
                                }
                            }
                        }
                        if(existe==false)
                        {
                            Toast.makeText(getApplicationContext(),"User Inválido!",Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            Intent intent = new Intent(this, SuccessActivity.class);
                            intent.putExtra("user", users);
                            startActivity(intent);
                        }
                    }
                    else//si no esta conectada a red
                    {
                        Toast.makeText(this,"Revisar Conexion!",Toast.LENGTH_SHORT).show();
                    }

                }else{
                    Toast.makeText(this,"Los Campos son Obligatorios!",Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }
}

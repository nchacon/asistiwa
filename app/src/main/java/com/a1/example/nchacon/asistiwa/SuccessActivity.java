package com.a1.example.nchacon.asistiwa;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class SuccessActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String DEBUG_TAG = "HttpExample";

    TextView tv, dia, registro;
    EditText et_fechainicio,et_fechafin;
    Button entrada, iniciacomida, terminacomida, salida,bbthoras,bbtacumuladas,bbtsalir;

    String fecha = java.text.DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());
    String actual = fecha.substring(0, 10);//obtener fecha
    String hora = fecha.substring(11, 19); //obtener hora
    //obtener usuario
    Intent inten;
    Bundle extras;
    String usr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success);

        tv = (TextView) findViewById(R.id.textView);
        inten = getIntent();
        extras = inten.getExtras();
        usr = (String) extras.get("user");
        tv.setText("Bienvenido " + usr);
        //useradmin Norberto
        String admin="Norberto";

        //textView
        dia = (TextView) findViewById(R.id.fecha);
        registro = (TextView) findViewById(R.id.entrada);

        //editText
        et_fechainicio=(EditText)findViewById(R.id.fechainicio);
        et_fechafin=(EditText)findViewById(R.id.fechafin);

        //botones
        entrada = (Button) findViewById(R.id.regEntrada);
        iniciacomida = (Button) findViewById(R.id.regInicioComida);
        terminacomida = (Button) findViewById(R.id.regTerminaComida);
        salida = (Button) findViewById(R.id.regSalida);
        bbthoras=(Button)findViewById(R.id.verhorasDia);
        bbtacumuladas=(Button)findViewById(R.id.acumuladas);
        bbtsalir=(Button)findViewById(R.id.salir);

        dia.setText("Dia: " + actual);

        //valores default para la busqueda de fechas
        et_fechainicio.setText(actual);
        et_fechafin.setText(actual);

        if(usr.equals(admin)){
            bbtacumuladas.setEnabled(true);

        }else{
            bbtacumuladas.setEnabled(false);
            et_fechafin.setEnabled(false);
            et_fechainicio.setEnabled(false);
        }

        entrada.setOnClickListener(this);
        iniciacomida.setOnClickListener(this);
        terminacomida.setOnClickListener(this);
        salida.setOnClickListener(this);
        bbthoras.setOnClickListener(this);
        bbtacumuladas.setOnClickListener(this);
        bbtsalir.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.regEntrada:
                //fecha en milisegundos
                long milisegundos = Calendar.getInstance().getTimeInMillis();
                //conversion a fecha
                final Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(milisegundos);
                final SimpleDateFormat sdfParser = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss", new Locale("ES"));
                String sTime = sdfParser.format(cal.getTime());

                usr = (String) extras.get("user");

                Log.d("registraEntrada", ""+ actual + "," + usr + "," + milisegundos);
                registro.setText("Hora Entrada: " + hora);

                new DownloadWebpageTask().execute("http://192.168.1.72:60/AsistiWA/Register?dia="+actual+"&user="+usr+"&entrada="+milisegundos);
                Log.d("link", ""+"http://192.168.1.72:60/AsistiWA/Register?dia="+actual+"&user="+usr+"&entrada="+milisegundos);
                Toast.makeText(getApplicationContext(), "fecha actual: " + fecha, Toast.LENGTH_LONG).show();
                break;

            case R.id.regInicioComida:
                //fecha en milisegundos
                long milisegundoss = Calendar.getInstance().getTimeInMillis();

                usr = (String) extras.get("user");

                Log.d("iniciaComida",""+actual+","+usr+","+milisegundoss);
                registro.setText("Hora Entrada: " + hora);

                new DownloadWebpageTask().execute("http://192.168.1.72:60/AsistiWA/Register/actualizarInicioComida?dia="+actual+"&user="+usr+"&entrada="+milisegundoss);
                break;

            case R.id.regTerminaComida:
                //fecha en milisegundos
                long milisegundos1 = Calendar.getInstance().getTimeInMillis();

                usr = (String) extras.get("user");
                Log.d("terminaComida",""+actual+","+usr+","+milisegundos1);
                registro.setText("Hora Entrada: " + hora);

                new DownloadWebpageTask().execute("http://192.168.1.72:60/AsistiWA/Register/actualizarFinComida?dia="+actual+"&user="+usr+"&entrada="+milisegundos1);
                break;

            case R.id.regSalida:
                //fecha en milisegundos
                long milisegundos2 = Calendar.getInstance().getTimeInMillis();

                usr = (String) extras.get("user");

                Log.d("registrasalida",""+actual+","+usr+","+milisegundos2);
                registro.setText("Hora Entrada: " + hora);

                new DownloadWebpageTask().execute("http://192.168.1.72:60/AsistiWA/Register/salida?dia="+actual+"&user="+usr+"&entrada="+milisegundos2);
                break;

            case R.id.verhorasDia:
                //cerrar dia y ver resumen

                String usr = (String) extras.get("user");

                Log.d("VerHoras",""+actual+","+usr);
                registro.setText("Hora Entrada: " + hora);

                new DownloadWebpageTask().execute("http://192.168.1.72:60/AsistiWA/Register/horas?dia="+actual+"&user="+usr);
                break;

            case R.id.acumuladas:
                String fecha_inicio=et_fechainicio.getText().toString();
                String fecha_fin=et_fechafin.getText().toString();
                new DownloadWebpageTask().execute("http://192.168.1.72:60/AsistiWA/Register/reportehoras?fecha_inicio="+fecha_inicio+"&fecha_fin="+fecha_fin);
                break;

            case R.id.salir:
                Intent intSalir = new Intent(this, MainActivity.class);
                startActivity(intSalir);
                break;
        }
    }

    //class

    private class DownloadWebpageTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {
            try {
                return downloadUrl(urls[0]);
            } catch (IOException e) {
                return "no se puede recuperar lapagina, o url no valida";
            }
            //return link;
        }

        @Override
        protected void onPostExecute(String result) {

            registro.setText(result);
            Toast.makeText(getApplicationContext(), result+"!!! ", Toast.LENGTH_LONG).show();

        }

        private String downloadUrl(String myurl) throws IOException {
            InputStream is = null;
            int len = 500;

            try {
                URL url = new URL(myurl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                conn.connect();
                int response = conn.getResponseCode();
                Log.d(DEBUG_TAG, "response is:" + response);
                is = conn.getInputStream();
                String contentAsString = readIt(is, len);
                return contentAsString;
            } finally {
                if (is != null) {
                    is.close();
                }
            }
        }
        // Reads an InputStream and converts it to a String.
        public String readIt(InputStream stream, int len) throws IOException, UnsupportedEncodingException {
            Reader reader = null;
            reader = new InputStreamReader(stream, "UTF-8");
            char[] buffer = new char[len];
            reader.read(buffer);
            return new String(buffer);
        }
    }
}